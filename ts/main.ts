/// <reference path='../d.ts/DefinitelyTyped/jquery/jquery.d.ts' />
'use strict';
declare var Vue:any;
declare var page:any;
interface Element{
	classList:any;
}
/*
interface Array<T>{
	shuffle():any;
}
Array.prototype.shuffle = function() {
    var i = this.length;
    while(i){
        var j = Math.floor(Math.random()*i);
        var t = this[--i];
        this[i] = this[j];
        this[j] = t;
    }
    return this;
}
*/
Vue.filter('timeFormat',function(time:number):string{
    var ss:string = ('0'+~~((time/1000)%60)).slice(-2);
    var mm:string = ('0'+~~((time/(1000*60))%60)).slice(-2);
    var hh:string = ('0'+~~(time/(1000*60*60))).slice(-2);
	var str:string = hh + ':' + mm + ':' + ss;
	if(Number(hh)>=24){
		str = '23:59:59';
	}
	var htmlStr = str.replace(/([0-9:])/g,"<span class='time-str-$1'>$1</span>");
	htmlStr = htmlStr.replace(/str-:/g,"str-colon");
	return htmlStr;
});
module HDK{
	export function capitaliseFirstLetter(str){
	    return str.charAt(0).toUpperCase() + str.slice(1);
	}
	export function toCamelCase(str){
	    return str.replace(/(\-([a-z]))/g, function($1,$2,$3){return $3.toUpperCase();});
	}
	export class Reversi {
		private _grids:any[] = [];
		grids:any[] = [];
		timeElapsed:any = {time:0,date:new Date(0)};
		sid:number;
		turn:number = 1;
		saveData:any[];
		constructor(){
			this.init();
		}
		init(){
			this.turn = 1;
			// 64個のgrid作成
			var row:number[][] = [];
			row[0] = [0,0,0,0,0,0,0,0];
			row[1] = [0,0,0,0,0,0,0,0];
			row[2] = [1,0,1,0,2,0,0,0];
			row[3] = [0,1,2,2,2,1,0,0];
			row[4] = [0,0,1,2,2,2,0,0];
			row[5] = [0,1,0,1,0,0,0,0];
			row[6] = [0,0,0,2,1,0,0,0];
			row[7] = [0,0,0,0,0,0,0,0];

			var obj:any;
			for(var i = 0;i<8;i++){
				for (var j = 0;j<8;j++){
					obj = {y:i,x:j,value:row[j][i],directions:[],pts:0};
					this._grids.push(obj);
				}
			}
			// 方向を入れる
			// 近い方から入れる
			this._grids.forEach((g)=>{
				// 上方向
				var ary:any = [];
				var x = g.x;
				var y = g.y;
				var index;
				while(y>0){
					y--;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
				// 右上方向
				ary = [];
				x = g.x;
				y = g.y;
				while(y>0&&x<7){
					y--;
					x++;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
				// 右方向
				ary = [];
				x = g.x;
				y = g.y;
				while(x<7){
					x++;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
				// 右下方向
				ary = [];
				x = g.x;
				y = g.y;
				while(y<7&&x<7){
					y++;
					x++;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
				// 下方向
				ary = [];
				x = g.x;
				y = g.y;
				while(y<7){
					y++;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
				// 左下方向
				ary = [];
				x = g.x;
				y = g.y;
				while(y<7&&x>0){
					y++;
					x--;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
				// 左方向
				ary = [];
				x = g.x;
				y = g.y;
				while(x>0){
					x--;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
				// 左上方向
				ary = [];
				x = g.x;
				y = g.y;
				while(y>0&&x>0){
					y--;
					x--;
					index = y*8+x;
					ary.push(this._grids[index]);
				}
				g.directions.push(ary);
			})
			// 着手可能マス探索
			this.getPuttableGrid(this.turn);
			// 内部値を公開値に同期
			this.updateGrids();
		}
		getPuttableGrid(ms){
			// 着手可能マスを探す
			// 隣接8マスに自分以外のマスがある
			var es = ms*-1+3;
			this._grids.filter((g)=>{return g.value === 0}).forEach((g)=>{
				var pts = 0;
				g.directions.forEach((dir)=>{
					var ary = dir.map((d)=>{return d.value});
					var imax = ary.indexOf(ms);
					for(var i=0;i<=imax;i++){
						if(ary[i]!==es){
							break;
						}
					}
					if(i===imax){
						pts += imax;
					}
				});
				g.pts = pts;
			})
		}
		searchInDepth(depth:number = 4){
			var myTurn = this.turn;
			var enTurn = myTurn*-1+3;
			// depthまで読んだ時最大個数になる手を指す
			var minMax = (node,ms,d):number=>{
				var best = -64;
				var isEnd = false;
				// 盤面を再現
				this._grids.forEach((g,i)=>{
					g.value = node[i];
				});
				// 応手のチェック
				this.getPuttableGrid(ms);
				var ary = this._grids.map((g)=>{return g.pts});
				var max = Math.max.apply(null,ary);
				if(max===0){ // パス
					// 相手の手番にしてもう一度応手のチェック
					ms = ms*-1+3;
					// 応手のチェック
					this.getPuttableGrid(ms);
					ary = this._grids.map((g)=>{return g.pts});
					max = Math.max.apply(null,ary);
					if(max === 0){
						// 2手パスなら終局
						isEnd = true;
					}
				}
				if(isEnd || d===0){// 最下段なら評価値を返す
					best = node.filter((v)=>{return v === myTurn}).length - node.filter((v)=>{return v === enTurn}).length;
					// 四隅の価値を5倍にする
					[0,7,56,63].forEach((i)=>{
						var v = node[i];
						if(v === myTurn){
							best += 4;
						} else if(v === enTurn){
							best -= 4;
						}
					})
					return best;
				}
				var es = ms*-1+3;
				// 応手でforを回す
				this._grids.filter((g)=>{return g.pts > 0}).forEach((g)=>{
					var index = this._grids.indexOf(g);
					this.putStone(index,ms);
					var localNode = this._grids.map((g)=>{return g.value});
					var val = minMax(localNode,es,d-1);
					if(ms === myTurn && best<val){
						best = val;
					}
					if(ms !== myTurn && best<-val){
						best = -val;
					}
					// 盤面を元に戻す
					this._grids.forEach((g,i)=>{
						g.value = node[i];
					});
				})
				return best;
			}
			// 現局面の応手のチェック
			this.getPuttableGrid(myTurn);
			// 現局面を保存
			var curNode = this._grids.map((g)=>{return g.value});
			// evaluationを初期化

			// 応手でforを回す
			this._grids.forEach((g)=>{g.evaluation = -64});
			this._grids.filter((g)=>{return g.pts > 0}).forEach((g)=>{
				// 盤面を再現
				this._grids.forEach((g,i)=>{
					g.value = curNode[i];
				});
				var index = this._grids.indexOf(g);
				this.putStone(index,myTurn);
				var node = this._grids.map((g)=>{return g.value});
				var evaluation = minMax(node,enTurn,depth);
				g.evaluation = evaluation;
			});
			// 現局面に戻す
			this._grids.forEach((g,i)=>{
				g.value = curNode[i];
			});
			this.getPuttableGrid(myTurn);
			this.updateGrids();
		}
		putStone(index,ms){
			var grid = this._grids[index];
			grid.value = ms;
			grid.pts = 0;
			var es = ms*-1+3;
			grid.directions.forEach((dir)=>{
				var ary = dir.map((d)=>{return d.value});
				var imax = ary.indexOf(ms);
				for(var i=0;i<=imax;i++){
					if(ary[i]!==es){
						break;
					}
				}
				if(i===imax){
					for(i=0;i<imax;i++){
						dir[i].value = ms;
					}
				}
			});
		}
		changeTurn(){
			this.turn = this.turn*-1+3;
		}
		updateGrids(){
			this.grids = this._grids.map((g)=>{return {value:g.value,pts:g.pts,evaluation:g.evaluation!==undefined?g.evaluation:0}});
		}
		move(index){
			this.putStone(index,this.turn);
			this.changeTurn();
			this.getPuttableGrid(this.turn);
			this.checkIfPass();
			this.updateGrids();
		}
		checkIfPass(){
			var ary = this._grids.map((g)=>{return g.pts});
			var max = Math.max.apply(null,ary);
			if(max===0){ // パス
				// 相手の手番にしてもう一度応手のチェック
				this.changeTurn();
				// 応手のチェック
				this.getPuttableGrid(this.turn);
				ary = this._grids.map((g)=>{return g.pts});
				max = Math.max.apply(null,ary);
				if(max === 0){
					// 2手パスなら終局
					alert('end');
				} else {
					alert('pass');
				}
			}
		}
	}
	export function ReversiInit(rvs:HDK.Reversi){
		var vue = new Vue({
			el:'body',
			data:{
				grids:rvs.grids,
				timeElapsed:rvs.timeElapsed,
				scene:0,
				isReady:false
			},
			ready:function(){
				this.isready = true;
			},
		    methods:{
				startPlaying:function(){
					this.scene=1;
				},
				onClickGrid:function(e){
					if(e.targetVM.pts>0){
						var index = this.grids.indexOf(e.targetVM.$data);
						rvs.move(index);
						this.grids = rvs.grids;
						// 白番ならAIが着手
						if(rvs.turn === 2){
							setTimeout(()=>{
								rvs.searchInDepth(3);
								this.grids = rvs.grids;
								var evals = this.grids.map((g)=>{return g.evaluation});
								var max = Math.max.apply(null,evals);
								var cand = this.grids.filter((g)=>{return g.evaluation === max && g.pts>0});
								var r = Math.floor(Math.random()*cand.length);
								index = this.grids.indexOf(cand[r]);
								rvs.move(index);
								this.grids = rvs.grids;
							},50)
						}
					}
				}
		    }
		});
	}
}