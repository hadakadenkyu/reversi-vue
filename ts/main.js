/// <reference path='../d.ts/DefinitelyTyped/jquery/jquery.d.ts' />
'use strict';
Array.prototype.shuffle = function () {
    var i = this.length;
    while (i) {
        var j = Math.floor(Math.random() * i);
        var t = this[--i];
        this[i] = this[j];
        this[j] = t;
    }
    return this;
};
Vue.filter('exists', function (cand, val) {
    var n = Number(val);
    return (cand.indexOf(n) !== -1) ? val : '';
}).filter('timeFormat', function (time) {
    var ss = ('0' + ~~((time / 1000) % 60)).slice(-2);
    var mm = ('0' + ~~((time / (1000 * 60)) % 60)).slice(-2);
    var hh = ('0' + ~~(time / (1000 * 60 * 60))).slice(-2);
    var str = hh + ':' + mm + ':' + ss;
    if (Number(hh) >= 24) {
        str = '23:59:59';
    }
    var htmlStr = str.replace(/([0-9:])/g, "<span class='time-str-$1'>$1</span>");
    htmlStr = htmlStr.replace(/str-:/g, "str-colon");
    return htmlStr;
});

/*
Vue.filter('choice',function(val:number):number{
var velocity:number;
if(val>=800){
velocity = 5;
} else if(val>=600){
velocity = 4;
} else if(val>=200){
velocity = 3;
} else if(val>=50){
velocity = 2;
} else {
velocity = 1;
}
return velocity;
}).filter('dateFormat',function(issued:string):string{
var time = Number(issued)*1000;
var d = new Date(time);
var year:number = d.getFullYear();
var month:number = d.getMonth()+1;
var date:number = d.getDate();
var hour:number = d.getHours();
var minute:string = ( '0' + d.getMinutes() ).slice( -2 );
return [year,month,date].join('/') + ' ' + [hour,minute].join(':');
});
*/
var TCG;
(function (TCG) {
    function capitaliseFirstLetter(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    TCG.capitaliseFirstLetter = capitaliseFirstLetter;
    function toCamelCase(str) {
        return str.replace(/(\-([a-z]))/g, function ($1, $2, $3) {
            return $3.toUpperCase();
        });
    }
    TCG.toCamelCase = toCamelCase;
    var Numpla = (function () {
        function Numpla() {
            this.grids = [];
            this.hlines = [];
            this.vlines = [];
            this.blocks = [];
            this.relatives = [];
            this.iter = 0;
            this.timeElapsed = { time: 0, date: new Date(0) };
            // 81個のgrid作成
            var g, b;
            var index = 0;
            for (var i = 0; i < 9; i++) {
                for (var j = 0; j < 9; j++) {
                    b = Math.floor(i / 3) * 3 + Math.floor(j / 3);
                    g = { x: j, y: i, block: b, cand: [], val: 0, answer: 0, visible: true, choice: [], index: index++ }; // 後置で入れる
                    this.grids.push(g);
                }
            }
            var x, y;
            for (i = 0; i < 9; i++) {
                this.hlines[i] = this.grids.filter(function (g) {
                    return (g.y === i);
                });
                this.vlines[i] = this.grids.filter(function (g) {
                    return (g.x === i);
                });
                this.blocks[i] = this.grids.filter(function (g) {
                    return (g.block === i);
                });
            }
            var ary;

            for (i = 0; i < 81; i++) {
                g = this.grids[i];
                ary = [];
                Array.prototype.push.apply(ary, this.blocks[g.block], this.vlines[g.x], this.hlines[g.y]);

                // 重複と自身を除く
                ary = ary.filter(function (x, i, self) {
                    return self.indexOf(x) === i && g !== x;
                });
                this.relatives.push(ary);
            }
        }
        Numpla.prototype.init = function () {
            this.grids.forEach(function (g) {
                g.cand = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                g.val = 0;
                g.visible = true;
            });

            // ブロック1,5,9にランダムで値を入れてしまう
            var candNum1 = [1, 2, 3, 4, 5, 6, 7, 8, 9].shuffle();
            var candNum2 = [1, 2, 3, 4, 5, 6, 7, 8, 9].shuffle();
            var candNum3 = [1, 2, 3, 4, 5, 6, 7, 8, 9].shuffle();
            for (var i = 0; i < 9; i++) {
                this.blocks[0][i].val = candNum1[i];
                this.blocks[4][i].val = candNum2[i];
                this.blocks[8][i].val = candNum3[i];
                this.blocks[0][i].cand = [];
                this.blocks[4][i].cand = [];
                this.blocks[8][i].cand = [];
            }
        };
        Numpla.prototype.narrowCandidates = function (cell) {
            var _this = this;
            var targetGrids;
            if (typeof cell !== 'undefined') {
                targetGrids = this.grids.filter(function (g) {
                    return g.visible === false && (g.x === cell.x || g.y === cell.y || g.block === cell.block);
                });
            } else {
                targetGrids = this.grids.filter(function (g) {
                    return g.val === 0;
                });
            }
            targetGrids.forEach(function (g) {
                var cand = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                var imax = cand.length;
                var ary = _this.relatives[g.index];
                var v;
                for (var i = imax - 1; i >= 0; i--) {
                    v = cand[i];
                    if (ary.some(function (e) {
                        return (e.val === v);
                    })) {
                        cand.splice(i, 1);
                    }
                }
                g.cand = cand;
            });
        };

        // 仮定法再帰は無い
        Numpla.prototype.solve = function () {
            var _this = this;
            var result;
            var emptyGrids = this.grids.filter(function (e) {
                return (e.val === 0);
            });
            emptyGrids.forEach(function (g) {
                var imax = g.cand.length;
                var ary = _this.relatives[g.index];
                ;
                var v;
                for (var i = imax - 1; i >= 0; i--) {
                    v = g.cand[i];
                    if (ary.some(function (e) {
                        return (e.val === v);
                    })) {
                        g.cand.splice(i, 1);
                    }
                }

                // 値が1つなら確定させる
                if (g.cand.length === 1) {
                    g.val = g.cand[0];
                    g.cand = [];

                    // 確定したら再帰
                    _this.solve();

                    // forEachは抜ける
                    return;
                }
            });
            var hasSettled = false;
            for (var i = 0; i < 9; i++) {
                // lone ranger
                var allCand;

                // ブロックを走査
                allCand = [];
                this.blocks[i].forEach(function (g) {
                    Array.prototype.push.apply(allCand, g.cand);
                });

                // 重複していないもののみをリスト
                var uniqueArray = allCand.filter(function (x, i, self) {
                    return self.indexOf(x) === i && i === self.lastIndexOf(x);
                });
                uniqueArray.forEach(function (v) {
                    // 1つしかない候補は確定
                    var uniqGrid = _this.blocks[i].filter(function (g) {
                        return (g.cand.indexOf(v) !== -1);
                    });
                    if (uniqGrid.length) {
                        uniqGrid[0].val2 = v;
                        uniqGrid[0].cand = [];
                        hasSettled = true;
                        return;
                        //console.log('Lone Ranger!!');
                    }
                });
                if (hasSettled) {
                    // 確定したら再帰
                    this.solve();
                }

                // vlineを走査
                allCand = [];
                this.vlines[i].forEach(function (g) {
                    Array.prototype.push.apply(allCand, g.cand);
                });

                // 重複していないもののみをリスト
                var uniqueArray = allCand.filter(function (x, i, self) {
                    return self.indexOf(x) === i && i === self.lastIndexOf(x);
                });
                uniqueArray.forEach(function (v) {
                    // 1つしかない候補は確定
                    var uniqGrid = _this.vlines[i].filter(function (g) {
                        return (g.cand.indexOf(v) !== -1);
                    });
                    if (uniqGrid.length) {
                        uniqGrid[0].val2 = v;
                        uniqGrid[0].cand = [];
                        hasSettled = true;
                        return;
                        //console.log('Lone Ranger!!');
                    }
                });
                if (hasSettled) {
                    // 確定したら再帰
                    this.solve();
                }

                // vlineを走査
                allCand = [];
                this.hlines[i].forEach(function (g) {
                    Array.prototype.push.apply(allCand, g.cand);
                });

                // 重複していないもののみをリスト
                var uniqueArray = allCand.filter(function (x, i, self) {
                    return self.indexOf(x) === i && i === self.lastIndexOf(x);
                });
                uniqueArray.forEach(function (v) {
                    // 1つしかない候補は確定
                    var uniqGrid = _this.hlines[i].filter(function (g) {
                        return (g.cand.indexOf(v) !== -1);
                    });
                    if (uniqGrid.length) {
                        uniqGrid[0].val2 = v;
                        uniqGrid[0].cand = [];
                        hasSettled = true;
                        return;
                        //console.log('Lone Ranger!!');
                    }
                });
                if (hasSettled) {
                    // 確定したら再帰
                    this.solve();
                }
            }
            ;
            return this.grids.every(function (e) {
                return (e.val !== 0);
            });
        };
        Numpla.prototype.compensate = function () {
            var _this = this;
            this.iter++;
            this.minLength = 9;

            // 確定してない場合候補から縦横ブロックに存在する値を除く
            var emptyGrids = this.grids.filter(function (e) {
                return (e.val === 0);
            });
            emptyGrids.forEach(function (g) {
                var imax = g.cand.length;
                var ary = _this.relatives[g.index];
                var v;
                for (var i = imax - 1; i >= 0; i--) {
                    v = g.cand[i];

                    // 自分を取り除く
                    if (ary.some(function (e) {
                        return (e.val === v);
                    })) {
                        g.cand.splice(i, 1);
                    }
                }

                // 値が1つなら確定させる
                if (g.cand.length === 1) {
                    g.val = g.cand[0];
                    g.cand = [];

                    // 確定したら再帰
                    _this.compensate();

                    // forEachは抜ける
                    return;
                } else {
                    _this.minLength = (g.cand.length > _this.minLength) ? _this.minLength : g.cand.length;
                }
            });

            // 候補数が少ないものを一つ確定させる
            if (this.minLength > 0) {
                var g = this.grids.filter(function (e) {
                    return (e.cand.length === _this.minLength);
                })[0];
                if (g) {
                    g.val = g.cand[Math.floor(Math.random() * this.minLength)];
                    g.cand = [];

                    // 確定したら再帰
                    this.compensate();
                    return;
                }
            } else if (this.grids.some(function (e) {
                return (e.val === 0);
            })) {
                this.init();
                this.compensate();
            } else {
                // 破綻チェックその2
                var flag = false;
                this.grids.forEach(function (g) {
                    var ary = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                    flag = g.some(function (g) {
                        return ary.indexOf(g.val) === -1;
                    });
                    if (flag) {
                        return;
                    }
                });
                if (flag) {
                    this.init();
                    this.compensate();
                }
            }
        };
        Numpla.prototype.startTimer = function () {
            var _this = this;
            this.timeElapsed.time = 0;
            this.sid = setInterval(function () {
                _this.timeElapsed.time += 1000;
            }, 1000);
        };
        Numpla.prototype.stopTimer = function () {
            clearInterval(this.sid);
        };
        Numpla.prototype.generate = function (emptyCount) {
            if (typeof emptyCount === "undefined") { emptyCount = 30; }
            this.iter = 0;

            // 一発目
            this.init();
            this.compensate();
            this.grids.forEach(function (g) {
                g.answer = g.val;
            });

            // 数字を1つずつ抜きながら破綻しないかを見る
            var r, res;
            var trueGrids;
            var cnt = 0;
            for (var i = 0; i < 100; i++) {
                trueGrids = this.grids.filter(function (e) {
                    return (e.visible === true);
                });
                r = Math.floor(Math.random() * trueGrids.length);
                trueGrids[r].visible = false;
                this.grids.filter(function (g) {
                    return (g.visible === false);
                }).forEach(function (g) {
                    g.val = 0;
                    g.cand = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                });
                res = this.solve();

                // 破綻したら元に戻す
                if (!res) {
                    trueGrids[r].visible = true;
                    this.grids.forEach(function (g) {
                        g.val = g.answer;
                    });
                } else {
                    cnt++;
                    if (cnt > emptyCount) {
                        break;
                    }
                }
            }

            // 隠すべきところの数字を隠す
            this.grids.filter(function (g) {
                return (g.visible === false);
            }).forEach(function (g) {
                g.val = 0;
            });

            // 絞込
            this.narrowCandidates();

            /*
            console.log(this.iter);
            console.log(this.vlines[1].map(function(g){return g.answer}));
            console.log(this.vlines[2].map(function(g){return g.answer}));
            console.log(this.vlines[3].map(function(g){return g.answer}));
            console.log(this.vlines[4].map(function(g){return g.answer}));
            console.log(this.vlines[5].map(function(g){return g.answer}));
            console.log(this.vlines[6].map(function(g){return g.answer}));
            console.log(this.vlines[7].map(function(g){return g.answer}));
            console.log(this.vlines[8].map(function(g){return g.answer}));
            console.log(this.vlines[9].map(function(g){return g.answer}));
            console.log(this.grids.filter(function(g){return !g.visible}).length);
            */
            console.log(this.grids.filter(function (g) {
                return !g.visible;
            }).length);
            var txt = '';
            this.grids.forEach(function (g) {
                txt += g.val;
            });
            console.log(txt);
            return this.grids;
        };
        return Numpla;
    })();
    TCG.Numpla = Numpla;
})(TCG || (TCG = {}));
//# sourceMappingURL=main.js.map
