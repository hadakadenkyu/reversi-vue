<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width,minimum-scale=1.0" name="viewport">
	<title>リバーシテスト</title>
	<link rel="stylesheet/less" href="./less/main.less">
	<script src="./lib/js/autoprefixer.js"></script>
	<script>
		localStorage.clear();
		less = {
			postProcessor:function(css){
				return autoprefixer.process(css).css;
			},
			logLevel:1
		}
	</script>
	<script src="./lib/js/less-1.7.4.min.js"></script>
	<script>
		less.watch();
	</script>
</head>
<?php
	$tapEvent = 'click';
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$isiPhone = (bool) strpos($ua,'iPhone');
	$isiPad = (bool) strpos($ua,'iPad');
	if($isiPad || $isiPhone){
		$tapEvent = 'touchstart';
	}
?>
<body>
	<main style="visibility:hidden;" v-style="visibility:(isReady)?'hidden':'visible'">
		<div class="gameBoard">
			<div  v-repeat="grids" v-class="" v-on="<?php echo $tapEvent ?>:onClickGrid" class="gameBoard__grid">
				<span class="gameBoard__stone gameBoard__stone--{{value}}" v-text="pts+ ', eval:' + evaluation"></span>
			</div>
		</div>
	</main>
	<script src="./lib/js/vue.min.js"></script>
	<script src="./js/main.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script>
	var rvs = new HDK.Reversi;
	HDK.ReversiInit(rvs);
	window.onload = function(){
		HDK.ReversiInit(rvs);
	}
	</script>
	
</body>
</html>